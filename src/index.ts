import xs, {Stream} from 'xstream';
import {Clipboard} from 'react-native';

export class ClipboardSource {
  constructor() {}

  getString(): Stream<string> {
    return xs.fromPromise(Clipboard.getString());
  }
}

export function makeClipboardDriver() {
  return function clipboardDriver(sink: Stream<string>): ClipboardSource {
    sink.addListener({
      next: content => {
        Clipboard.setString(content);
      },
    });

    return new ClipboardSource();
  };
}
