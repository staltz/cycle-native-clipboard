# Cycle Native Clipboard

**A Cycle.js Driver for interacting with React Native's [Clipboard](https://facebook.github.io/react-native/docs/clipboard)**

```
npm install cycle-native-clipboard
```

## Usage

### Sink

Stream of strings that should be "set" on the clipboard.

### Source

Object with one method:

```js
{
  getString(): Stream<string>
}
```

The stream emits only once. To get a new string, call the method again to get a new stream of string.

## License

Copyright (C) 2018 Andre 'Staltz' Medeiros, licensed under MIT license
